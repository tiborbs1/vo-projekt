# VO projekt

Projekt za predmet Virtualna okruženja 2021./2022.

Predaja projekta: **petak 31.12.2021.**

[Tutorial za Unity](https://www.youtube.com/playlist?list=PLPV2KyIb3jR5QFsefuO2RlAgWEz6EvVi6)

Izvještaj [link za Google doc](https://docs.google.com/document/d/1AFjG4qtpAv4wUgWtuRJ3jymEARVVZo83/edit?usp=sharing&ouid=113013440732331853968&rtpof=true&sd=true)

## TO DO LISTA ZA GAME
Rok: 5.12.2021.
- [x] stvori projekt - 3D
- [x] teren - [unity terrain](https://www.youtube.com/watch?v=MWQv2Bagwgk&t=597s&ab_channel=Brackeys), asset store
- [x] auto - asset store (+ kontrole da vozi preko strelica)
- [x] finish line - trigger svaki put kad pređe liniju
- [x] VICTORY natpis

Rok: 12.12.2021.
- [x] jedan auto strelice drugi awsd (zasad -> kasnije joystick)
- [x] lighting podesiti nekako ako se može
- [x] preimenovati PLAY gumb u START
- [x] podesiti parametre da se lakše vozi -> napisati novu jednostavniju skriptu??
- [x] editanje ceste i svijeta - podignuti tlo tako da ne može zapeti izvan ceste
- [x] collectables - pokupi npr. plavu kutiju i vozi brže 3 sekunde 
- [ ] ~~dim ili vatra da se vidi ubrzanje - [particle system](https://www.youtube.com/watch?v=FEA1wTMJAR0&t=283s&ab_channel=Brackeys)~~
- [x] main menu - [START, HOW TO PLAY, QUIT](https://www.youtube.com/watch?v=zc8ac_qUXQY&t=641s&ab_channel=Brackeys)
- [x] drugi auto - split screen
- [x] konfeti kad pređe cilj - particle system
- [x] pomakni FINISH (tako da bude kraća vožnja)

Rok: 19.12.2021.
- [x] početak utrke - odbrojava 3,2,1,GO! -> ne mogu se kretati prije toga - script za vožnju je disabled po defaultu pa tek kasnije enabled kao komponenta auta
- [x] START -> svaki igrač upiše ime -> odbrojavanje -> vozi auto
- [x] timer - pokazuje vrijeme koliko dugo traje utrka -> na kraju piše za pobjednika: Your time is...
- [x] glazba
- [ ] ~~highscore~~
- [ ] ~~drugi auto se vozi preko joysticka~~
- [ ] ~~stavit u how to play za joystick gumb~~
- [x] BUILD
- [ ] VIDEO snimiti - screen record
- [ ] IZVJEŠTAJ - konačna verzija (+ prezentacija i video)

## IZVJEŠTAJ
- [x] 1. kratki uvod
- [x] 2. pregled područja i problematike uz kratki opis aktivnosti u svijetu (Što već postoji?)
- [ ] 3. opis napravljenog zajedno s postignutim rezultatima te diskusijom istih (Što smo mi napravili?)
- [x] 4. kratki zaključak (Je li sve napravljeno? Koja su poboljšanja moguća?)
- [ ] 5. literatura (Koje knjige, časopise i WWW stranice smo koristili?)
- [x] 6. poveznica na repozitorij (može biti OneDrive, Dropbox, Google Drive, GitHub...) u kojem se nalaze implementacija (uključujući build) i kratki video o projektu. Poveznica treba ostati aktivna do kraja semestra
- [ ] 7. kao dodatak dodati i popis svih napisanih programa zajedno sa kratkim uputama kako se programi koriste

## -----------------------------------------------------------------------------------------------------------------------------


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/tiborbs1/vo-projekt.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:9148812312a423e35adbb9d9cc56be83?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

