using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNames : MonoBehaviour
{
    public Text name1, name2;
    public GameObject player1_input, player2_input;
    public GameObject enter, names;

    public int countdownTime = 3;
    public Text count;

    public GameObject car1, car2;

   

    public void StoreName()
    {
        string n1 = player1_input.GetComponent<Text>().text;
        string n2 = player2_input.GetComponent<Text>().text;

        if (n1.Length == 0) n1 = "Player 1";
        if (n2.Length == 0) n2 = "Player 2";

        name1.text = n1;
        name2.text = n2;

        enter.SetActive(false);
        names.SetActive(true);

        Debug.Log("start countdown");
        StartCoroutine(CountdownToStart());

    }
    IEnumerator CountdownToStart()
    {
        while (countdownTime > 0)
        {
            Debug.Log(countdownTime.ToString());
            count.text = countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }
        count.text = "GO!";
        yield return new WaitForSeconds(1f);
        Destroy(count);


        StartRace();
    }

    public void StartRace()
    {
        car1.GetComponent<LPPV_CarController>().enabled = true;
        car2.GetComponent<LPPV_CarController>().enabled = true;
        car1.GetComponent<FlipCar>().enabled = true;
        car2.GetComponent<FlipCar>().enabled = true;

        this.GetComponent<Timer>().enabled = true;
    }
}
