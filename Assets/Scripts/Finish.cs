using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Finish : MonoBehaviour
{

    public Text victory;
    public GameObject over;
    public Text player1_name, player2_name;

    public Text timerText;
    public Text finishTime;

    public GameObject car1, car2;
    public GameObject cam1, cam2;

    public GameObject gm;
    public ParticleSystem particles1;
    public ParticleSystem particles2;

    public bool win = false;

    public GameObject finishLine;


    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        if (!win)
        {
            Debug.Log(win);

            string tag = other.tag.ToString();
            string name = "Player 1";
            if (tag == "RED")
            {
                Debug.Log(tag);
                name = player1_name.text;
                cam1.GetComponent<LPPV_Follow>().followBehind = false;

                win = true;
            }
            else if (tag == "GREEN")
            {
                name = player2_name.text;
                cam2.GetComponent<LPPV_Follow>().followBehind = false;
            }

            Collider col = finishLine.GetComponent<Collider>();
            col.isTrigger = false;


            gm.GetComponent<Timer>().enabled = false;

            particles1.Play();
            particles2.Play();




            // wait for x seconds
            StartCoroutine(Wait());


            victory.text = name + " won!";



            finishTime.text = "your time is " + timerText.text;
        }

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        over.SetActive(true);
    }
}