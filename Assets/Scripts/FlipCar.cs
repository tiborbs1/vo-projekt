using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlipCar : MonoBehaviour
{
    public GameObject car;
    [SerializeField] private String flipButton;

    void Update()
    { 

        if (Input.GetButtonDown(flipButton))
        {
            float z = car.transform.localEulerAngles.z;

            float newZ = -z;

            //if (z < 0) newZ = -z;
            //else newZ = 360 - z;

            car.transform.Rotate(0, 0, newZ);
        }

    }
}
