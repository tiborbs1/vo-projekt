using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public Text timer;
    float start;

     public bool finish = false;

    // Start is called before the first frame update
    void Start()
    {
        start = Time.time;
    }

    // Update is called once per frame
    void Update()
    {

        

        float time = Time.time - start;

        int minutes = (int)(time / 60);
        int seconds = (int)(time % 60);

        string m="00", s="00";

        if (minutes < 10)
        {
            m = "0" + minutes.ToString();
        }
        else m = minutes.ToString();

        if (seconds < 10)
        {
            s = "0" + seconds.ToString();
        }
        else s = seconds.ToString();

        timer.text = m + ":" + s;


    }
}
