using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public GameObject pickupEffect;
    
    public float multiplier = 1.3f;
    public float duration = 10.0f;

    private float oldSpeed = 0.0f;
    private LPPV_CarController car = null; 

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "RED" && other.gameObject.tag != "GREEN") return;

        car = other.gameObject.GetComponent<LPPV_CarController>();

        Instantiate(pickupEffect, transform.position, transform.rotation);

        StartCoroutine(ActivatePowerUp(other.gameObject.tag));

    }

    IEnumerator ActivatePowerUp(string tag)
    {

        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        oldSpeed = car.getOriginalTopSpeed();

        car.setTopSpeed(multiplier * oldSpeed);

        Debug.LogWarning(tag + "--> current-speed: " + car.getTopSpeed());

        yield return new WaitForSeconds(10f);

        car.setTopSpeed(oldSpeed);

        Debug.LogWarning(tag + "--> current-speed: " + car.getTopSpeed());

        GetComponent<Renderer>().enabled = true;
        GetComponent<Collider>().enabled = true;

    }

    


}
